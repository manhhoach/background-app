import { TypeOrmModuleAsyncOptions, TypeOrmModuleOptions } from '@nestjs/typeorm';

export const typeOrmConfig: TypeOrmModuleAsyncOptions = {
  useFactory: async (): Promise<TypeOrmModuleOptions> => {
    return {
      type: 'mssql',
      host: process.env.DB_HOST || 'localhost',
      port: parseInt(process.env.DB_PORT) || 3306,
      username: process.env.DB_USERNAME || 'root',
      password: process.env.DB_PASSWORD || 'manh123456',
      database: process.env.DB_DATABASE || 'mysql',
      entities: [__dirname + '/../**/**/*.entity{.ts,.js}'],
      synchronize: true,
      logging: true,
      options: {
        trustServerCertificate: true
      }
    };
  },
};
