export enum RoleEnum {
    USER = 1,
    SHOP_KEEPER = 2,
    SHOP_OWNER = 3,
    ADMIN = 4,
}