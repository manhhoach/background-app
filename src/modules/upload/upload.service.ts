import { Injectable } from '@nestjs/common';
import toStream from 'buffer-to-stream';
import { UploadApiErrorResponse, UploadApiResponse, v2 } from 'cloudinary';

@Injectable()
export class UploadService {
   async uploadImage(
      file: any,
   ): Promise<UploadApiResponse | UploadApiErrorResponse> {

      return new Promise((resolve, reject) => {
         const upload = v2.uploader.upload_stream((error, result) => {
            if (error) return reject(error);
            resolve(result);
         });

         toStream(file.buffer).pipe(upload);
      });
   }
}
