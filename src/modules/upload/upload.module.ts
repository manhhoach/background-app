import { Module } from '@nestjs/common';
import { CloudinaryProvider } from 'src/config/cloudinary.provider';
import { UploadController } from './upload.controller';
import { UploadService } from './upload.service';

@Module({
  controllers: [UploadController],
  providers: [UploadService, CloudinaryProvider],
  exports: [UploadService]
})
export class UploadModule { }
