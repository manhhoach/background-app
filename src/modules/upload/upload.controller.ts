import { BadRequestException, Controller } from '@nestjs/common';
import { UploadService } from './upload.service';

@Controller('upload')
export class UploadController {
   constructor(private uploadService: UploadService) { }

   async uploadImageToCloudinary(file: any) {
      return await this.uploadService.uploadImage(file).catch(() => {
         throw new BadRequestException('Invalid file type.');
      });
   }
}
