import { Body, Controller, Delete, Get, HttpStatus, Param, ParseIntPipe, Patch, Post, Query } from '@nestjs/common';
import { customController } from 'src/common/custom-controller';
import { SkipAuth } from '../auth/skip.auth.decorator';
import { CreateImageDto } from './dto/create-image.dto';
import PaginateImage from './dto/paginate-image.dto';
import { UpdateImageDto } from './dto/update-image.dto';
import { ImageService } from './image.service';

@SkipAuth()
@Controller('image')
export class ImageController {

   constructor(private imageService: ImageService) { }

   @Get()
   getAll(@Query() query: PaginateImage) {
      return customController(this.imageService.paginate(query), HttpStatus.OK);
   }

   @Get(':id')
   getById(@Param('id', ParseIntPipe) id: number) {
      return customController(this.imageService.findById(id), HttpStatus.OK);
   }

   @Post()
   create(@Body() body: CreateImageDto) {
      return customController(this.imageService.create(body), HttpStatus.CREATED)
   }

   @Patch(':id')
   update(@Param('id', ParseIntPipe) id: number, @Body() body: UpdateImageDto) {
      return customController(this.imageService.update(id, body), HttpStatus.OK);
   }

   @Delete(':id')
   delete(@Param('id', ParseIntPipe) id: number) {
      return customController(this.imageService.delete(id), HttpStatus.OK);
   }
}
