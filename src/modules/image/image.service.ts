import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { BaseService } from 'src/common/base/base.service';
import { Repository } from 'typeorm';
import { ImageEntity } from './image.entity';

@Injectable()
export class ImageService extends BaseService<ImageEntity> {
   constructor(@InjectRepository(ImageEntity) private readonly imageRepository: Repository<ImageEntity>) {
      super(imageRepository);
   }
}
