import {
   BaseEntity,
   Column,
   CreateDateColumn,
   Entity,
   PrimaryGeneratedColumn
} from 'typeorm';

@Entity({
   name: 'images',
})
export class ImageEntity extends BaseEntity {
   @PrimaryGeneratedColumn()
   id: number;

   @Column()
   description: string;

   @Column()
   file_path: string;

   @CreateDateColumn()
   created_date: Date;

   @Column({ default: 0 })
   num_of_downloads: number;

   @Column({ default: 0 })
   num_of_views: number;

   @Column({ nullable: true })
   user_id: number;

}
