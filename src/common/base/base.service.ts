import { NotFoundException } from '@nestjs/common';
import { BaseEntity, DeepPartial, FindOptionsOrder, FindOptionsWhere, Repository } from 'typeorm';
import CommonPaginate from '../paginate';
import { getPagination, getPagingData } from './../../utils/paginate';

export abstract class BaseService<TEntity extends BaseEntity> {
  constructor(private readonly repository: Repository<TEntity>) { }

  create(entity: DeepPartial<TEntity>) {
    const data = this.repository.create(entity);
    return this.repository.save(data);
  }

  async paginate(commonPage: CommonPaginate) {
    const { skip, limit } = getPagination(commonPage.page_size, commonPage.page_index);
    const data = await this.repository.findAndCount({
      skip: skip,
      take: limit,
      where: commonPage.condition,
      order: commonPage.order ? commonPage.order : { created_date: 'DESC' },
    });
    return getPagingData(data, commonPage.page_index, limit);
  }

  findAll(condition?: FindOptionsWhere<TEntity> | FindOptionsWhere<TEntity>[], order?: FindOptionsOrder<TEntity>) {
    let orderDefault = { created_date: 'DESC' } as unknown as FindOptionsOrder<TEntity>;
    return this.repository.find({ where: condition ? condition : {}, order: order ? order : orderDefault });
  }

  async findById(id: number) {
    const data = await this.repository.findOneBy({
      id,
    } as unknown as FindOptionsWhere<TEntity>);
    if (!data) throw new NotFoundException();
    return data;
  }

  async update(id: number, entity: DeepPartial<TEntity>) {
    const data = await this.findById(id);
    return this.repository.save(Object.assign(data, entity));
  }

  async delete(condition: number | number[] | FindOptionsWhere<TEntity>) {
    await this.repository.delete(condition);
    return null;
  }
}
