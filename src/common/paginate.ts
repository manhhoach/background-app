import { Type } from "class-transformer";
import { IsInt } from "class-validator";

export default class CommonPaginate {
   @Type(() => Number)
   @IsInt()
   page_index: number;

   @Type(() => Number)
   @IsInt()
   page_size: number;

   condition?: any;
   order?: any;
}